require 'test_helper'

class AboutsControllerTest < ActionController::TestCase
  test "should get student" do
    get :student
    assert_response :success
  end

  test "should get teacher" do
    get :teacher
    assert_response :success
  end

  test "should get price" do
    get :price
    assert_response :success
  end

  test "should get lesson" do
    get :lesson
    assert_response :success
  end

end
