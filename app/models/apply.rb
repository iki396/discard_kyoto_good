class Apply
  include ActiveModel::Model

  # oz/app/models/student.rb
  # http://qiita.com/k-shogo/items/5bbc23e1d0dd0ad3a8a2
  # 
  # classname    | 説明              | required?
  # ----------------------------------------------
  # :name          生徒名
  # :sex           生徒性別             y
  # :email         連絡先アドレス
  # :tel_num       連絡先電話番号        y
  # :school        学校名
  # :grade         学年[学校]           y
  # :year          学年[学年]           y
  # :purpose       通塾目的             y
  # ----------------------------------------------
  # :like_subject  好きな教科
  # :weak_subject  苦手な教科
  # :deviation     偏差値
  # :result        成績
  # :club          部活
  # :lesson        習い事
  # ----------------------------------------------
  # :school_choice 受験志望校
  # :hope          塾に求めること
  # :problem       悩み
  # :willingness   学習意欲
  # :personality   生徒の性格
  # ----------------------------------------------
  # :lesson_style  希望する授業スタイル
  # :teacher_sex   講師の性別指定
  # :extension     居残り授業の可否
  # :message       その他
  
  attr_accessor :name, :sex, :email, :tel_num, :school, :grade, :year, :purpose, 
    :like_subject, :weak_subject, :deviation, :result, :club, :lesson,
    :school_choice, :hope, :problem, :willingness, :personality,
    :lesson_style, :teacher_sex, :extension, :message
  
  validates :tel_num, :presence => {:message => '.tell_num'}
  validates :sex,     :presence => {:message => '.sex'}
  validates :grade,   :presence => {:message => '.grade'}
  validates :purpose, :presence => {:message => '.purpose'}

end