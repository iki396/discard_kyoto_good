$ ->
  $('label.selected').click ->
    $(@).next('.select_box_wrapper').fadeIn()
    
  $('input[type="radio"]').change ->
    if $('.select_box_wrapper').is(':visible')
      $('.select_box_wrapper:visible').prev('label.selected').text($(@).next('label').text())
      $('.select_box_wrapper').fadeOut()