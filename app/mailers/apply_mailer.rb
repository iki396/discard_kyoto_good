# encoding: utf-8
class ApplyMailer < ActionMailer::Base
  include Roadie::Rails::Automatic
  
  default from: "earth660@gmail.com"
  
  def received_email(apply)
    @apply = apply
    mail(:to => "earth660@gmail.com", :subject => '問い合わせ')
  end
end
