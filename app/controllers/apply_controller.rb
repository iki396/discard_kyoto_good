class ApplyController < ApplicationController
  def index
    #入力画面表示
    @apply = Apply.new
    render :action => 'index'
  end

  def confirm
    # 入力値のチェック
    @apply = Apply.new(params[:apply])
    if @apply.valid?
      # OK
      render :action => 'confirm'
    else
      # NG
      render :action => 'index'
    end
  end

  def thanks
    # send mail
    @apply = Apply.new(params[:apply])
    ApplyMailer.received_email(@apply).deliver

    render :action => 'thanks'
  end
end