module ActionView
  module Helpers
    module ActiveModelInstanceTag
      def tag_generate_errors?(options)
        (options['type'] != 'hidden' && options['type'] != 'radio')
      end
    end
  end
end